
resource "aws_instance" "projectboot" {
  ami           = "ami-0fc5d935ebf8bc3bc"
  instance_type = "t2.micro"
  key_name = "terraform"
  security_groups = ["allow_ssh","allow_http","allow_egress"]
  user_data = file("script.sh")

  tags = {
    Name = "produçao"
  }
}