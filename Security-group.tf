resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "permitir acesso remoto via porta 22"

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }

  tags = {
    Name = "allow_ssh"
  }
}

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "permitir acesso remoto via porta 80"

  ingress {
    description      = "http from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }

  tags = {
    Name = "allow_http"
  }
}  

resource "aws_security_group" "allow_egress" {
  name        = "allow_egress"
  description = "permitir acesso remoto via porta"

    egress {
     from_port        = 0
     to_port          = 0
     protocol         = "-1"
     cidr_blocks      = ["0.0.0.0/0"]
     
    
    }

  tags = {
    Name = "allow_egress"
  }
}  